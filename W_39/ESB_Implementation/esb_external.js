var ESB = require('light-esb-node')

function esbCallback(error, message){
    if(error){
        console.log("Encountered an error... ", error);
    }
    else{
        console.log("Processing...");
        console.log("Message: ", message);
    }
}


var component = ESB.createLoggerComponent(esbCallback);
var receiver1 = ESB.createCallComponent(esbCallback, "https://jsonplaceholder.typicode.com/users", "get");
var receiver2 = ESB.createLoggerComponent(esbCallback);
var payload = {
    greeting: "Hello World"
};

var messageEsb = ESB.createMessage(payload, "Bogdan", "Browser - Chrome", "some id");

component.connect(receiver1);
receiver1.connect(receiver2);

component.send(messageEsb);
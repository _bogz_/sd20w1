var ESB = require('light-esb-node');
const sqlite3 = require('sqlite3');

const readline = require('readline-sync');
let db = new sqlite3.Database('greetings.sqlite');

// callback function

function esbCallback(error, message){
    if(error){
        console.log('Error while processing the message\n', error, message);
    }
    else{
        console.log('Message received...');
        console.log('Message: ', message);
        console.log('Processing')
    }
}

// entry component = this will simply log the payload 
let component = ESB.createLoggerComponent(esbCallback);
let database_writer = ESB.createScriptComponent(esbCallback, function(esbMessage, callback){
    console.log("Message is", esbMessage);
    db.run(`INSERT INTO greeting(SenderUser, SenderMachine, SenderID, Message) VALUES (?,?,?,?)`, 
        [esbMessage.payload.user, 'CRM', "test", esbMessage.payload.test_greeting], function(err) {
            if(err){
                return;
            }
            console.log('A record was inserted in the database');
        });
    db.close();
});

component.connect(database_writer);

// program logic ... stubs mostly


let name = readline.question("Name: ");
let greeting = readline.question("Greeting: ");
let example_message = {
    user: name,
    test_greeting: greeting
};

let message = ESB.createMessage(example_message, "bogdan", "CRM", "SomeID");

// Simply sending the message to the main component that will then send it to the receiver1 and then receiver 2
component.send(message);

// it does not have a database. So it will most likely fail.
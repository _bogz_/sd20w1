var ESB = require('light-esb-node')

function esbCallback(error, message){
    if(error){
        console.log("Encountered an error... ", error);
    }
    else{
        console.log("Processing...");
        console.log("Message: ", message);
    }
}


var component = ESB.createLoggerComponent(esbCallback);

let payload = {
    name: "Bob",
    email: "bob@bob.com"
};

let message = ESB.createMessage(payload, "Someone", "Something", "Some id")

var receiver1 = ESB.createScriptComponent(esbCallback, (esbMessage, esbCallback)=> {
    console.log(esbMessage.payload.name);
    esbMessage.payload.additional = "Hello world";

    console.log(esbMessage.payload);

});

var receiver2 = ESB.createLoggerComponent(esbCallback);

component.connect(receiver1);
receiver1.connect(receiver2);


component.send(message);
import requests
import json
import sqlite3

db = sqlite3.connect('lib_ex.sqlite')
url = 'http://localhost:8080/generate-isbn'

def validate_input(inp):
    while inp.lower() != 'y' and inp.lower() != 'n':
        inp = input("Try again.. [Y/n]: ")
    if inp.lower() == 'y':
        return True
    else: 
        return False

def add_book(title, author, year):
    response = requests.post(url, json={'title': title, 'author': author, 'year': year})
    data = response.json()

    query = 'INSERT INTO books VALUES(?,?,?,?)'
    db.execute(query, [title, author, year, data['isbn']])
    db.commit()


print("Welcome to my awesome library system...")
inp = input("Would you like to add a book? [Y/n] ")

while True:
    flag = validate_input(inp)
    if flag:
        title = input("Title: ")
        author = input("Author: ")
        year = input("Year: ")
        add_book(title, author, year)
        inp = input("Add another? ")
    else:
        break

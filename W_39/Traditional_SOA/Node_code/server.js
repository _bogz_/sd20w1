const { response } = require('express');
const express = require('express');

var app = express();
// Without the next line, I would not be able to read the json body
app.use(express.json());


app.get('/test', (req, res) => {
    res.status(200).send({message: "Server is up and running... "});
});


app.post('/generate-isbn', (req, res) => {
    let isbn = {
        isbn: `ISBN-1234-${req.body.title.charCodeAt(0)}-${req.body.year}`
    }
    res.status(200).send(isbn);
});



app.listen(8080, (err) => {
    if(err)
    {
        console.log(err)
        return;
    }
    console.log("Listening on port 8080");
});
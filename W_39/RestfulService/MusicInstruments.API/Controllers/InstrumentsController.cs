using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;
using MusicInstruments.API.Models;
using MusicInstruments.API.Models.Requests;
using MusicInstruments.API.Services.InstrumentService;

namespace MusicInstruments.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class InstrumentsController : ControllerBase
    {
        private readonly ILogger<InstrumentsController> _logger;
        private readonly IInstrumentRepository _instrumentsRepository;

        public InstrumentsController(ILogger<InstrumentsController> logger, IInstrumentRepository instrumentRepository)
        {
            _logger = logger;
            _instrumentsRepository = instrumentRepository;
        }

        /// <summary>Create Instrument</summary>
        /// <param name="createPersonRequest">Model</param>
        [HttpPost]
        [ProducesResponseType(typeof(Instrument), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Instrument>> CreateInstrument([FromBody, BindRequired] CreateInstrumentRequest request)
        {
            try
            {
                var instrument = await _instrumentsRepository.CreateInstrumentAsync(request);
                return Created(string.Empty, instrument);
            }
            catch(ArgumentException e){
                return BadRequest(e);
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>Get Instrument by ID</summary>
        /// <param name="id">Model</param>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Instrument), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Instrument>> GetInstrument([FromRoute, BindRequired] int id)
        {
            try
            {
                var person = await _instrumentsRepository.GetInstrumentAsync(id);
                return Ok(person);
            }
            catch(NullReferenceException e){
                return NotFound(e);
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>Get all</summary>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Instrument>), 200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<IEnumerable<Instrument>>> GetAll()
        {
            try
            {
                var instruments = await _instrumentsRepository.GetAllAsync();
                return Ok(instruments);
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>Update an Instrument</summary>
        /// <param name="request">Model</param>
        [HttpPut]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult> UpdateInstrument([FromBody, BindRequired] UpdateInstrumentRequest request)
        {
            try
            {
                await _instrumentsRepository.UpdateInstrumentAsync(request);
                return NoContent();
            }
            catch(ArgumentException e){
                return BadRequest(e);
            }
            catch(NullReferenceException e){
                return NotFound(e);
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }


        /// <summary>Delete Instrument by ID</summary>
        /// <param name="id">Model</param>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult> DeleteInstrument([FromRoute, BindRequired] int id)
        {
            try
            {
                await _instrumentsRepository.DeleteInstrumentAsync(id);
                return NoContent();
            }
            catch(NullReferenceException e){
                return NotFound(e);
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

    }
}

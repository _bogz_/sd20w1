using System.Collections.Generic;
using System.Threading.Tasks;
using MusicInstruments.API.Models;
using MusicInstruments.API.Models.Requests;

namespace MusicInstruments.API.Services.InstrumentService
{
    public interface IInstrumentRepository
    {
        Task<Instrument> CreateInstrumentAsync(CreateInstrumentRequest request);
        Task<Instrument> GetInstrumentAsync(int id);
        Task<IEnumerable<Instrument>> GetAllAsync();
        Task UpdateInstrumentAsync(UpdateInstrumentRequest request);
        Task DeleteInstrumentAsync(int id);
    }
}
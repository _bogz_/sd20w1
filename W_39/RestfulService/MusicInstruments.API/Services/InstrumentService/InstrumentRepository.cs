using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MusicInstruments.API.Models;
using MusicInstruments.API.Models.Requests;

namespace MusicInstruments.API.Services.InstrumentService
{
    public class InstrumentRepository : IInstrumentRepository
    {
        private MusicalInstrumentsDbContext _dbContext;

        public InstrumentRepository(MusicalInstrumentsDbContext dbContext)
           => _dbContext = dbContext;

        public async Task<Instrument> CreateInstrumentAsync(CreateInstrumentRequest request)
        {
            if(request.Price <= 0 || string.IsNullOrEmpty(request.Name))
                throw new ArgumentException("It seems that you are missing some properties on the made request");
            
            var instrument = new Instrument{
                Name = request.Name,
                Price = request.Price
            };
            await _dbContext.AddAsync(instrument);
            await _dbContext.SaveChangesAsync();
            return instrument;
        }

        public async Task DeleteInstrumentAsync(int id)
        {
            var instrument = await _dbContext.Instruments.FirstOrDefaultAsync(_ => _.Id == id);
            if(instrument == null)
                throw new NullReferenceException("The requested item does not exist in the database.");

            _dbContext.Instruments.Remove(instrument);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Instrument>> GetAllAsync()
            => await _dbContext.Instruments.ToListAsync();

        public async Task<Instrument> GetInstrumentAsync(int id)
        {
            var instrument = await _dbContext.Instruments.FirstOrDefaultAsync( _ => _.Id == id);

            if(instrument == null)
                throw new NullReferenceException("Item does not exist in the database...");

            return instrument;
        }

        public async Task UpdateInstrumentAsync(UpdateInstrumentRequest request)
        {
            if(request.Price <= 0 || string.IsNullOrEmpty(request.Name) || request.Id == 0)
                throw new ArgumentException("It seems that you are missing some properties on the made request");
            
            var instrument = await _dbContext.Instruments.FirstOrDefaultAsync(_ => _.Id == request.Id);
            if(instrument == null){
                throw new NullReferenceException("Item does not exist in the database");
            }

            instrument.Name = request.Name;
            instrument.Price = request.Price;

            await _dbContext.SaveChangesAsync();
        }
    }
}
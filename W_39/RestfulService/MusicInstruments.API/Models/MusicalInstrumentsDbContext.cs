using Microsoft.EntityFrameworkCore;

namespace MusicInstruments.API.Models
{
    public class MusicalInstrumentsDbContext : DbContext
    {
        public DbSet<Instrument> Instruments{get;set;}

        protected override void OnConfiguring(DbContextOptionsBuilder options)
           => options.UseSqlite("Data Source=../instruments.sqlite");
    }
}
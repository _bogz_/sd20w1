namespace MusicInstruments.API.Models.Requests
{
    public class CreateInstrumentRequest
    {
        public string Name {get;set;}
        public float Price {get;set;}
    }

    public class UpdateInstrumentRequest
    {
        public int Id {get;set;}
        public string Name {get;set;}
        public float Price {get;set;}
    }
}
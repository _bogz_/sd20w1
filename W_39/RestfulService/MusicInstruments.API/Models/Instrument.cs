namespace MusicInstruments.API.Models
{
    public class Instrument
    {
        public int Id {get;set;}
        public string Name {get;set;}
        public float Price {get;set;}
    }
}
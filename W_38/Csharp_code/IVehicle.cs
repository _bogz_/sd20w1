namespace Csharp_code{

    public interface IVehicle{
        
        void start_engine();
        void stop_engine();
        int drive(int no_of_km);
    }

}
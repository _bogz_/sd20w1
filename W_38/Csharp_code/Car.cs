using System;

namespace Csharp_code
{
    public class Car : IVehicle
    {
        private bool Running{get;set;}
        private int Killometers{get;set;}
        public string Brand {get;set;}

        public int drive(int no_of_km)
        {
            if(!Running){
                //throw new InvalidOperationException("Start your car first, sir");
                Console.WriteLine("Car is stopped");
            }
            Killometers += no_of_km;
            Console.WriteLine($"The car drove for {no_of_km}");
            return Killometers;
        }

        public void start_engine()
        {
            if(Running){
                Console.WriteLine("Car is running already...");
                return;
            }
            Running = true;
            Console.WriteLine("Car is now running  - VROOM");
        }

        public void stop_engine()
        {
            if(!Running){
                Console.WriteLine("Your car is stopped already...");
                return;
            }
            Running = false;
            Console.WriteLine("Stopped now...");
        }

        public int KillometersDriven()
            => Killometers;
    }
}
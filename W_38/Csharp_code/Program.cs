﻿using System;

namespace Csharp_code
{
    class Program
    {
        static void Main(string[] args)
        {
            var car1 = new Car{
                Brand = "Batmobil"
            };

            car1.start_engine();
            car1.drive(20);
            car1.stop_engine();
            car1.drive(509);
            Console.WriteLine(car1.KillometersDriven());


            var truck = new Truck{
                ManufacturingYear = "1990"
            };

            truck.start_engine();
            truck.drive(20);
            truck.stop_engine();
            truck.drive(509);
            truck.LoadTruck(1000);
            Console.WriteLine(truck.GetWeight());
        }
    }
}

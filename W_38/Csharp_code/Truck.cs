using System;

namespace Csharp_code
{
    public class Truck : IVehicle
    {
        public string ManufacturingYear {get;set;}
        private bool Running{get;set;}
        private int Killometers{get;set;}
        private int Weight{get;set;}

        public int drive(int no_of_km)
        {
            if(!Running){
                throw new InvalidOperationException("Start your truck first, sir");
            }
            Killometers += no_of_km;
            Console.WriteLine($"The truck drove for {no_of_km}");
            return Killometers;
        }

        public void start_engine()
        {
            if(Running){
                Console.WriteLine("Truck is running already...");
                return;
            }
            Running = true;
            Console.WriteLine("Truck is now running  - VROOM");
        }

        public void stop_engine()
        {
            if(!Running){
                Console.WriteLine("Your Truck is stopped already...");
                return;
            }
            Running = false;
            Console.WriteLine("Stopped now...");
        }

            
        public int GetWeight()
            => Weight;
        
        public void LoadTruck(int weight){
            Weight = weight;
            Console.WriteLine(Weight);
        }
    }
}
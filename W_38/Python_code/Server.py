from JsonProcesor import JsonProcessor
from CsvProcessor import CsvProcessor

# json_processor = JsonProcessor()

# json_processor.write_file("people.json", {'name':'bob'})
# json_processor.read_file("people.json")

# complex_object = {
#     'name': "stuff",
#     'age': 12,
#     'email': "something or something else"
# }
# print(complex_object)
# json_processor.serialize_store("complex.json", json_processor.read_file('people.json'))

# json_processor.desirialize("test.json")

csv_processor = CsvProcessor()

csv_processor.read_file('csv_ex.csv')

csv_object = {'name': ['Raphael', 'Something'],
                    'age': [19, 22],
                    'note': ['big', 'green']}

csv_processor.write_file("csv_write.csv", csv_object)

csv_processor.serialize_store('serialized_content.msgpack', csv_object)

csv_processor.desirialize("test")
import pandas as pd
from abc import ABCMeta, abstractmethod
from IFileProcesor import IFileProcessor
import msgpack

class CsvProcessor(IFileProcessor):
    @classmethod
    def read_file(self, file_name):
        df = pd.read_csv(file_name)
        for index, row in df.iterrows():
            print(f"Name: {row['Name']}, LastName: {row['Surname']}, Note: {row['Note']}")


    @classmethod
    def write_file(self, file_name, content):
        df = pd.DataFrame(content)
        df.to_csv(file_name, index=False)

    
    @classmethod 
    def serialize_store(self, file_name, content):
        with open(file_name, "wb") as outfile:
            packed = msgpack.packb(content)
            outfile.write(packed)


from abc import ABCMeta, abstractmethod


class IFileProcessor:

    __metaclass__ = ABCMeta

    """Reads a file and prints it to the terminal"""
    @abstractmethod
    def read_file(self, file_name):
        raise NotImplementedError

    """Write to a file"""
    @abstractmethod
    def write_file(self, file_name, content):
        raise NotImplementedError

    """Serialize the content and store it in a file"""
    @abstractmethod 
    def serialize_store(self, file_name, content):
        raise NotImplementedError

    """Desirialize the content of a file - Msgpack and print it on the console."""
    @abstractmethod
    def desirialize(self, file_name):
        raise NotImplementedError

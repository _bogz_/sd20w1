# Import stuff for the database
# Get the cursor
# Get the input from the keyboard
# Create the insert statement
# Commit it - Done
import sqlite3
db = sqlite3.connect('people.sqlite')
print('Welcome to this lovely registration system')

db_cursor = db.cursor()
name = input("Name: ")
email = input("Email: ")

insert_command = '''INSERT INTO person VALUES(?,?)'''

db_cursor.execute(insert_command, (name, email))
db.commit()
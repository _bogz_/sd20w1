const readline = require('readline-sync');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const csvWriter = createCsvWriter({
    path: 'people_write.csv',
    header:[
        {id: 'name', title: 'NAME'},
        {id: 'age', title: 'AGE'},
        {id: 'note', title: 'NOTE'},
    ]
})

const records = [];

let add_p = add_person();



while(add_p){
    let name = readline.question('What is your name?');
    let age = readline.question('What is your age?');
    let note = readline.question('What note do you want to leave?')

    records.push({name: name, age: age, note: note});

    add_p = add_person()
}

csvWriter.writeRecords(records)
    .then(() => {console.log('...Done')})


function add_person(){
    let add_person = '';
    while(add_person.toLowerCase() != 'y' && add_person.toLowerCase() != 'n'){
        add_person = readline.question('Wanna add anoher entry? [Y/n]: ');
    }
    
    if(add_person.toLowerCase() == 'y')
        return true;
    else
        return false;
}
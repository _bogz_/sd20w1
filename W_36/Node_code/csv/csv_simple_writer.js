const readline = require('readline-sync');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const csvWriter = createCsvWriter({
    path: 'people_w.csv',
    header:[
        {id: 'name', title: 'NAME'},
        {id: 'age', title: 'AGE'},
        {id: 'note', title: 'NOTE'},
    ]
})

const records = [];



function add_more(){
    let add_p = "";
    while(add_p.toLowerCase() != "y" && add_p.toLowerCase() != "n")
        add_p = readline.question("Add a new person to this file? [y/n]");
    
    if(add_p.toLowerCase() == 'y')
        return true;
    else 
        return false;
}

let add_person = add_more();
while(add_person){
    let name = readline.question("Name: ");
    let age = readline.question("Age: ");
    let note = readline.question("Note: ");

    records.push({name:name, age:age, note:note});
    add_person = add_more();
}

csvWriter.writeRecords(records)
    .then(() => {console.log('...Done')})


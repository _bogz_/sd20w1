# Task 3 done

import sqlite3
import json

db = sqlite3.connect('people.sqlite')

with open('person.json', 'r') as file_json:
    person = json.load(file_json)
    print(person)
    db_cursor = db.cursor()
    db_insert = """INSERT INTO person(Name, Email, Address, Phone) VALUES(?,?,?,?)"""
    data = [person['name'], person['email'], person['address'], person['phone']]
    print(data)
    db_cursor.execute(db_insert, data)
    db.commit()
    print("DONE")
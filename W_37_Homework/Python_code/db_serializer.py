import sqlite3
import  msgpack

db = sqlite3.connect('people.sqlite')

db_cursor = db.cursor()

select_all = """SELECT * FROM person"""

people = []

for row in db_cursor.execute(select_all):
    people.append({'name': row[1], 'email': row[2], 'address': row[3], 'phone': row[4]})
with open('serialized_database.msgpack', 'wb') as f_msgpack:
    msgpack.dump(people, f_msgpack)

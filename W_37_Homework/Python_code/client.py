# First, second task

import json

name = input("Your name is? ")
email = input("Your email is? ")
address = input("Address is? ")
phone = input("Phone is? ")

person = {
    'name': name,
    'email': email,
    'address': address,
    'phone': phone
}

with open('person.json', 'w') as f_json:
    json.dump(person, f_json)
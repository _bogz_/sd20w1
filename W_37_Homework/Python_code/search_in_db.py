import sqlite3

db = sqlite3.connect('people.sqlite')

db_cursor = db.cursor()

select_specific = """SELECT * FROM person WHERE Name = ?"""

name_to_search = input("What name are you searching for?")

for row in db_cursor.execute(select_specific, [name_to_search]):
    print({'name': row[1], 'email': row[2], 'address': row[3], 'phone': row[4]})


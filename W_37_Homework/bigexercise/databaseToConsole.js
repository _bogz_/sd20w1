const sqlite3 = require('sqlite3').verbose();
const prompt = require('prompt-sync')();
const fs = require("fs");
const msgpack = require("msgpack");

let db = new sqlite3.Database('./db/people.db', (err) => {
    if(err) {
        return console.log(err.message);
    }
    console.log("Connected to database!")
});

let sql = `SELECT * FROM people`;
let databaseContent = [];
const search = prompt('Search for a person based on name/email/address/phoneNumber: ');


db.all(sql, [], (err, rows) => {
    if (err) {
        return console.log(err);
    }
    rows.forEach((row) => {
        // console.log("People:", row);
        databaseContent.push(row);
        if(search === row.name || search === row.email || search === row.address || search === row.phoneNumber) {
            console.log(`${row.name}:`, row);
        }
    });
    let peopleToSerialize = msgpack.pack(databaseContent);
    fs.writeFileSync("people.msgpack", peopleToSerialize);

    let data = fs.readFileSync('people.msgpack');
    let peopleToDeserialize = msgpack.unpack(data);
    console.log("Deserialized:", peopleToDeserialize);
});

db.close();

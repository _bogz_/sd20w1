const fs = require("fs");
const prompt = require('prompt-sync')();

const name = prompt('Write your name: ');
const email = prompt('Write your email: ');
const address = prompt('Write your address: ');
const phoneNumber = prompt('Write your phone number: ');

let obj = {
    name,
    email,
    address,
    phoneNumber
};

let people = JSON.stringify(obj);

console.log("people: ", people);
fs.writeFileSync("people.json", people);
const fs = require("fs");

let data = fs.readFileSync('people.json');
let people = JSON.parse(data);

console.log("People:", people);

const sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('./db/people.db', (err) => {
    if(err) {
        return console.log(err.message);
    }
    console.log("Connected to database!")
});

// Create the people table
db.run(`CREATE TABLE IF NOT EXISTS people(
    id integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    name text NOT NULL UNIQUE,
    email text NOT NULL UNIQUE,
    address text,
    phoneNumber text)`
);

db.run(`INSERT INTO people(name, email, address, phoneNumber) VALUES(?, ?, ?, ?)`, [people.name, people.email, people.address, people.phoneNumber], (err) => {
    if (err) {
        return console.log(err.message);
    }
    console.log(`A new row has been inserted!`);
});

db.close();
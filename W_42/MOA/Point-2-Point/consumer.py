import pika
import time
import json

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost')
)

channel = connection.channel()

channel.queue_declare(queue='sd20w1', durable=True)

print("The queue has been successfully initialized")

def callback(ch, method, properties, body):
    print(" [x] Received %r" % body.decode())
    response = json.loads(body)
    print(response['a'] + response['b'])
    time.sleep(body.count(b'.'))
    print(" [x] Done")
    ch.basic_ack(delivery_tag=method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='sd20w1', on_message_callback=callback)

channel.start_consuming()
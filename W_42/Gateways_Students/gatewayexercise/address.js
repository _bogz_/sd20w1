const express = require("express");
const bodyParser = require('body-parser');
const sqlite3 = require('sqlite3').verbose();
const server = express();
const PORT = 3001;
/**
 * Gateway:
 * Username: cons
 * Key id: b49b0be3-d3ce-42b6-9c2b-ca2cb8566ef6
 * Key secret: 0JnHlDwQbFtl29ZIAVj4SZ:0sSjH9MUrnsYnkNxb4p212
 */


server.use(bodyParser.json());

let db = new sqlite3.Database('../db/users.db', (err) => {
    if(err) {
        return console.log(err.message);
    }
    console.log("Connected to database!")
});

db.run(`CREATE TABLE IF NOT EXISTS Address(
    id integer PRIMARY KEY AUTOINCREMENT,
    streetNumber text NOT NULL,
    floor text NOT NULL,
    zip text NOT NULL,
    city text NOT NULL,
    country text NOT NULL,
    lives INTEGER DEFAULT 0,
    userID INTEGER,
    FOREIGN KEY(userID) REFERENCES User(id))`
);

server.post("/user/address/:userId", (req, res) => {
    let address = req.body;
    let sql = `INSERT INTO Address(streetNumber, floor, zip, city, country, userId) VALUES(?, ?, ?, ?, ?, ?)`;

    console.log("Address:", address);
    console.log("req.params.userId", req.params.userId);
    db.all(`SELECT * FROM User WHERE id = ?`, [req.params.userId], (err, row) => {
        if (err) {
            console.log(err);
        }
        console.log("User: ", row);
        if(!row.length) {
            res.status(404).json({
                message: `No user found with the id ${req.params.userId}!`
            });
        } else {
            db.run(sql, [address.streetNumber, address.floor, address.zip, address.city, address.country, req.params.userId], (err) => {
                if (err) {
                    res.status(400).json({
                        message: 'The address could not be created!',
                        error: err.message
                    });
                    console.log(err.message);
                }
                console.log(`A new row has been inserted!`);
                res.status(201).json({
                    message: 'Address successfully created!',
                    address
                });
            });
        }
    });
});

server.get("/address", (req, res) => {
    let sql = `SELECT * FROM Address`;
    db.all(sql, [], (err, addresses) => {
        if (err) {
            res.status(400).json({
                message: 'The addresses could not be showed!',
                error: err
            });
            console.log(err);
        }

        console.log("Addresses:", addresses);
        res.status(200).json({
            addresses
        });
    });
});

server.get("/user/address/:userId", (req, res) => {
    let sql = `SELECT * FROM Address WHERE userID = ?`;
    db.all(`SELECT * FROM User WHERE id = ?`, [req.params.userId], (err, row) => {
        if (err) {
            console.log(err);
        }
        console.log("User: ", row);
        if(!row.length) {
            res.status(404).json({
                message: `No user found with the id ${req.params.userId}!`
            });
        } else {
            db.all(sql, [req.params.userId], (err, addresses) => {
                if (err) {
                    res.status(400).json({
                        message: 'The addresses could not be showed!',
                        error: err
                    });
                    console.log(err);
                }

                console.log("Addresses:", addresses);
                res.status(200).json({
                    addresses
                });
            });
        }
    });
});

server.get("/address/:id", (req, res) => {
    console.log("req.params.id: ", req.params.id);
    let sql = `SELECT * FROM Address WHERE id = ?`;

    db.all(sql, [req.params.id], (err, address) => {
        if (err) {
            console.log(err);
        }
        console.log("Address: ", address);
        if(address.length) {
            res.status(200).json({
                address
            });
        } else {
            res.status(404).json({
                message: `No address found with the id ${req.params.id}!`
            });
        }
    });
});

server.put("/address/:id", (req, res) => {
    console.log("req.params.id: ", req.params.id);
    console.log("req.body: ",  req.body);
    let address = req.body;
    let sql = `UPDATE Address SET streetNumber = ?, floor = ?, zip = ?, city = ?, country = ? WHERE id = ?`;
    db.all(`SELECT * FROM Address WHERE id = ?`, [req.params.id], (err, row) => {
        if (err) {
            console.log(err);
        }
        console.log("Address: ", row);
        if(!row.length) {
            res.status(404).json({
                message: `No address found with the id ${req.params.id}!`
            });
        } else {
            db.run(sql, [address.streetNumber, address.floor, address.zip, address.city, address.country, req.params.id], (err) => {
                if (err) {
                    res.status(400).json({
                        message: 'The address could not be updated!',
                        error: err.message
                    });
                    console.log(err.message);
                }
                res.status(201).json({
                    message: 'Address successfully updated!',
                    address
                });
            });
        }
    });
});

server.delete("/address/:id", (req, res) => {
    console.log("req.params.id: ", req.params.id);
    let sql = `DELETE FROM Address WHERE id=?`;
    db.all(`SELECT * FROM Address WHERE id = ?`, [req.params.id], (err, row) => {
        if (err) {
            console.log(err);
        }
        console.log("Address: ", row);
        if(!row.length) {
            res.status(404).json({
                message: `No address found with the id ${req.params.id}!`
            });
        } else {
            db.run(sql, req.params.id, (err) => {
                if (err) {
                    res.status(400).json({
                        message: 'The user could not be deleted!',
                        error: err.message
                    });
                    console.log(err.message);
                }
                res.status(201).json({
                    message: 'Address successfully deleted!',
                });
            });
        }
    });
});

server.patch("/address/:id", (req, res) => {
    console.log("req.params.id: ", req.params.id);
    console.log("req.body: ",  req.body);
    let address = req.body;
    let sql = `UPDATE Address SET userID = ? WHERE id = ?`;
    db.all(`SELECT * FROM Address WHERE id = ?`, [req.params.id], (err, row) => {
        if (err) {
            console.log(err);
        }
        console.log("Address: ", row);
        if(!row.length) {
            res.status(404).json({
                message: `No address found with the id ${req.params.id}!`
            });
        } else {
            db.run(sql, [address.userID, req.params.id], (err) => {
                if (err) {
                    res.status(400).json({
                        message: 'The address could not be updated!',
                        error: err.message
                    });
                    console.log(err.message);
                }
                res.status(201).json({
                    message: 'Address successfully changed!',
                });
            });
        }
    });
});

server.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

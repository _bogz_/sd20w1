const path = require('path');
const gateway = require('express-gateway');
require('../user');
require('../address');

gateway()
  .load(path.join(__dirname, 'config'))
  .run();

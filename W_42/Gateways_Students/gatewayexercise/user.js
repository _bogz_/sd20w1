const express = require("express");
const bodyParser = require('body-parser');
const sqlite3 = require('sqlite3').verbose();
const server = express();
const PORT = 3000;
/**
 * Gateway:
 * Username: cons
 * Key id: b49b0be3-d3ce-42b6-9c2b-ca2cb8566ef6
 * Key secret: 0JnHlDwQbFtl29ZIAVj4SZ:0sSjH9MUrnsYnkNxb4p212
 */


server.use(bodyParser.json());

let db = new sqlite3.Database('../db/users.db', (err) => {
    if(err) {
        return console.log(err.message);
    }
    console.log("Connected to database!")
});

db.run(`CREATE TABLE IF NOT EXISTS User(
    id integer PRIMARY KEY AUTOINCREMENT,
    name text NOT NULL UNIQUE,
    email text NOT NULL UNIQUE,
    phoneNumber text NOT NULL,
    gender text)`
);

server.post("/user", (req, res) => {
    let user = req.body;
    let sql = `INSERT INTO User(name, email, phoneNumber, gender) VALUES(?, ?, ?, ?)`;

    console.log("User:", user);
    db.run(sql, [user.name, user.email, user.phoneNumber, user.gender], (err) => {
        if (err) {
            res.status(400).json({
                message: 'The user could not be created!',
                error: err.message
            });
            console.log(err.message);
        }
        console.log(`A new row has been inserted!`);
        res.status(201).json({
            message: 'User successfully created!',
            user
        });
    });
});

server.get("/users", (req, res) => {
    let sql = `SELECT * FROM User`;
    db.all(sql, [], (err, users) => {
        if (err) {
            res.status(400).json({
                message: 'The users could not be showed!',
                error: err
            });
            console.log(err);
        }

        console.log("Users:", users);
        res.status(200).json({
            users
        });
    });
});

server.get("/user/:id", (req, res) => {
    console.log("req.params.id: ", req.params.id);
    let sql = `SELECT * FROM User WHERE id = ?`;

    db.all(sql, [req.params.id], (err, user) => {
        if (err) {
            console.log(err);
        }
        console.log("User: ", user);
        if(user.length) {
            res.status(200).json({
                user
            });
        } else {
            res.status(404).json({
                message: `No user found with the id ${req.params.id}!`
            });
        }
    });
});

server.put("/user/:id", (req, res) => {
    console.log("req.params.id: ", req.params.id);
    console.log("req.body: ",  req.body);
    let user = req.body;
    let sql = `UPDATE User SET name = ?, email = ?, phoneNumber = ?, gender = ? WHERE id = ?`;
    db.all(`SELECT * FROM User WHERE id = ?`, [req.params.id], (err, row) => {
        if (err) {
            console.log(err);
        }
        console.log("User: ", row);
        if(!row.length) {
            res.status(404).json({
                message: `No user found with the id ${req.params.id}!`
            });
        } else {
            db.run(sql, [user.name, user.email, user.phoneNumber, user.gender, req.params.id], (err) => {
                if (err) {
                    res.status(400).json({
                        message: 'The user could not be updated!',
                        error: err.message
                    });
                    console.log(err.message);
                }
                res.status(201).json({
                    message: 'User successfully updated!',
                    user
                });
            });
        }
    });
});

server.delete("/user/:id", (req, res) => {
    console.log("req.params.id: ", req.params.id);
    let sql = `DELETE FROM User WHERE id=?`;
    db.all(`SELECT * FROM User WHERE id = ?`, [req.params.id], (err, row) => {
        if (err) {
            console.log(err);
        }
        console.log("User: ", row);
        if(!row.length) {
            res.status(404).json({
                message: `No user found with the id ${req.params.id}!`
            });
        } else {
            db.run(sql, req.params.id, (err) => {
                if (err) {
                    res.status(400).json({
                        message: 'The user could not be deleted!',
                        error: err.message
                    });
                    console.log(err.message);
                }
                res.status(201).json({
                    message: 'User successfully deleted!',
                });
            });
        }
    });
});

server.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

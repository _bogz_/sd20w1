import msgpack

with open("movie.msgpack", "rb") as data_file:
    byte_data = data_file.read()
    data_loaded = msgpack.unpackb(byte_data)
    print(data_loaded)


# Write msgpack file
with open("movie_2.msgpack", "wb") as outfile:
    packed = msgpack.packb(data_loaded)
    outfile.write(packed)
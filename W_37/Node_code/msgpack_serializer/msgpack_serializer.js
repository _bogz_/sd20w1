const request = require('request');
const fs = require('fs');
const msgpack = require('msgpack');

request('http://www.omdbapi.com/?i=tt3896198&apikey=f6708f35', { json: true }, (err, res, body) => {
  if (err) 
    return console.log(err); 
  console.log(res.body);
  fs.writeFileSync('movies.msgpack', msgpack.pack(res.body));
});

let movie = {};

fs.readFile('movies.msgpack', null, (err, data) => {
    if(err){
        console.log(err);
        return;
    }
    movie = msgpack.unpack(data);
    console.log(movie);
});
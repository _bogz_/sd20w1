const fs = require('fs');
const proto = require('protobufjs');
const readline = require('readline-sync');

let name = readline.question('Your name is... ');
let email = readline.question('Your email is... ');
let age = readline.question('Your age is... ');
let greeting = readline.question('Your greeting is... ');

console.log(`Hello there ${name}! Your age is ${age} and you can be contacted on ${email}. \nYour greeting is ${greeting}`);

let obj = {
    name: name,
    email: email,
    age: Number(age),
    greeting: greeting,
    anotherProp: true
};


proto.load('example.proto', (err, root)=> {
    if(err){
        console.log(err);
        return;
    }

    var Person = root.lookupType('exampleperson.Person');

    var errObj1 = Person.verify(obj);
    if(errObj1){
        console.log(errObj1);
        console.log("Errored");
    }

    var encodedMessage = Person.encode(obj).finish();

    var decodedMessage = Person.decode(encodedMessage);
    console.log(decodedMessage);

})
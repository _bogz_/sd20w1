const readline = require('readline-sync');

let date_input = readline.question("Please enter your date here ([dd/MM/yyyy HH:mm:ss]): ");

let isoDate = new Date(date_input).toISOString();

console.log(isoDate.toString("yyyy-mm-dd hh:mm"));

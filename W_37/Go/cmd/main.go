package main

import (
	"exercise-w37/internal"
	"github.com/sirupsen/logrus"
	"os"
)

/*
TASKS:

1. Build a system that takes 4 inputs from keyboard
	(name, email, address, phone number)
2. It should store them in a file called people.json
3. Build another system that reads this file and saves the data in a database
4. The third system should simply iterate over the items from the database
	and print people on demand
5. Bonus - allow the user to search for a person based on
	a string (phone, email, name, address)
6. Use json serializer/pickle/protobuf/msqpack to serialize the content
	of the database and store it in a file
*/

const fileName = "people"
const fileType = ".json"
const fileSourceDirectory = "assets/"

func main(){
	// configure logger
	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.InfoLevel)

	// 1.
	var userData = internal.GetUserData()
	logrus.Infof("Inputs received %s", userData)

	// 2.
	filePath := fileSourceDirectory + fileName + fileType
	internal.WriteUserDataToFile(userData, filePath)

	// 3.
	db := internal.OpenDBConnection(fileName + ".db")

	// Create the table from our struct.
	db.AutoMigrate(&internal.UserData{})

	// add userData to database
	// internal.CreateEntry(db, userData)
	db.Save(&userData)

	// add static person user data to the database
	db.Save(&internal.UserData{
		Name:        "Person",
		Email:       "person@example.com",
		Address:     "placeholder",
		PhoneNumber: "+4917600000000",
	})

	// 4. get all db entries and print them to the console
	userDataSet := internal.GetAllDBEntries(db, true)

	// 5. find person in the db and print person data to the console
	personUserData := internal.GetElementFromDB(db, "Name", "Person")
	logrus.Println("Person UserData ", personUserData.Name)

	// 6.
	fileNameProto := fileName + ".proto"
	internal.EncodeMessage(fileNameProto, userDataSet)
	userDataSetDecoded := internal.DecodeMessage(fileNameProto)
	logrus.Info(userDataSetDecoded)
}

package internal

import (
	"github.com/sirupsen/logrus"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

// OpenDBConnection open a connection to the referenced database file
func OpenDBConnection(dbFile string) *gorm.DB {
	db, err := gorm.Open(sqlite.Open(dbFile), &gorm.Config{})
	if err != nil {
		logrus.Fatal(err.Error())
	}
	return db
}

// GetAllDBEntries fetches all entries in the referenced database and prints them
func GetAllDBEntries(db *gorm.DB, printEntries bool) UserDataSet {
	var userDataSet []UserData
	db.Find(&userDataSet)
	if printEntries {
		logrus.Info("Printing UserData entries")
		for _, userData := range userDataSet {
			logrus.Infof("UserData: %s", userData.Name)
		}
	}
	return UserDataSet{UserData: userDataSet}
}

// GetElementFromDB search an element by attribute
func GetElementFromDB(db *gorm.DB, attribute string, searchedElement string) UserData {
	var userData UserData
	// search for the element and return the first one found
	db.Where(attribute + " = ?", searchedElement).First(&userData)
	return userData
}

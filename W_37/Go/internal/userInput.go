package internal

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

// 1. Build a system that takes 4 inputs from keyboard
func GetUserData() UserData {
	return UserData{
		Name:        getUserInput("Enter Your name"),
		Email:       getUserInput("Enter Your email"),
		Address:     getUserInput("Enter Your address"),
		PhoneNumber: getUserInput("Enter Your phone number"),
	}
}


// get user input from the console and return it
func getUserInput(description string) string {
	fmt.Println(description + ": ")
	var variable string
	_, err := fmt.Scanln(&variable)
	if err != nil {
		log.Error(err)
	}
	return variable
}

// structs
type UserData struct {
	// adds ID, CreatedAt, UpdatedAt and DeletedAt to the struct
	gorm.Model
	Name 		string `json:"name"`
	Email 		string `json:"email"`
	Address 	string `json:"address"`
	PhoneNumber string `json:"phone_number"`
}

type UserDataSet struct {
	UserData []UserData `json:"user_data"`
}



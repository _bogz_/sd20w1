package internal

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
)

const FileMode = 0644

// WriteUserDataToFile writes UserData to json document > people.json
func WriteUserDataToFile(userData UserData, filePath string) {
	// read file contents
	data := ReadUserDataToFile(filePath)

	// add entry to file
	data.UserData = append(data.UserData, userData)

	// Preparing the data to be marshalled and written.
	dataBytes, err := json.Marshal(data)
	if err != nil {
		logrus.Error(err)
	}

	err = ioutil.WriteFile(filePath, dataBytes, FileMode)
	if err != nil {
		logrus.Error(err)
	}
}

// ReadUserDataToFile reads UserData from json document
func ReadUserDataToFile(filePath string) UserDataSet {
	err := checkFile(filePath)
	if err != nil {
		logrus.Error(err)
	}
	file, err := ioutil.ReadFile(filePath)
	if err != nil {
		logrus.Error(err)
	}
	var data UserDataSet
	err = json.Unmarshal(file, &data)
	if err != nil {
		logrus.Fatal(err.Error())
	}

	return data
}

// checkFile create file if it does not exist
func checkFile(filePath string) error {
	_, err := os.Stat(filePath)
	if os.IsNotExist(err) {
		_, err := os.Create(filePath)

		err = ioutil.WriteFile(filePath, []byte("{}"), FileMode)
		if err != nil {
			logrus.Error(err)
		}
		if err != nil {
			return err
		}
	}
	return nil
}

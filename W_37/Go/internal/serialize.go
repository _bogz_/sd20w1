package internal

import (
	protoInternal "exercise-w37/internal/peopleProto"
	"github.com/golang/protobuf/proto"
	"github.com/sirupsen/logrus"
	"io/ioutil"
)

// EncodeMessage encode UserDataSet to proto byte structure in fileName
func EncodeMessage(fileName string, userDataSet UserDataSet) {
	userDataSetProto := translateToUserDataProtoList(userDataSet)
	// Write the new address book back to disk.
	out, err := proto.Marshal(userDataSetProto)
	if err != nil {
		logrus.Fatalln("Failed to encode address book:", err)
	}
	if err := ioutil.WriteFile(fileName, out, FileMode); err != nil {
		logrus.Fatalln("Failed to write address book:", err)
	}
	logrus.Info("UserDataSet Encoded")
}

// DecodeMessage decode fileName to UserDataSet
func DecodeMessage(fileName string) UserDataSet {
	// Read the existing UserDataSet
	fileData, err := ioutil.ReadFile(fileName)
	if err != nil {
		logrus.Fatalln("Error reading file:", err)
	}
	var userDataSet protoInternal.UserDataSetProto
	if err := proto.Unmarshal(fileData, &userDataSet); err != nil {
		logrus.Fatalln("Failed to parse address book:", err)
	}
	return translateToUserDataSet(&userDataSet)
}


// HELPER METHODS TO TRANSLATE STRUCTS

// translateToUserDataProto translate UserData to people_proto.UserDataProto
func translateToUserDataProto(userData UserData) *protoInternal.UserDataProto {
	return &protoInternal.UserDataProto{
		Name:        userData.Name,
		Email:       userData.Email,
		Address:     userData.Address,
		PhoneNumber: userData.PhoneNumber,
	}
}

// translateToUserData translate people_proto.UserDataProto to UserData
func translateToUserData(userDataProto *protoInternal.UserDataProto) UserData {
	return UserData{
		Name:        userDataProto.Name,
		Email:       userDataProto.Email,
		Address:     userDataProto.Address,
		PhoneNumber: userDataProto.PhoneNumber,
	}
}

// translateToUserDataProtoList translate UserDataSet to []people_proto.UserDataProto
func translateToUserDataProtoList(userDataSet UserDataSet) *protoInternal.UserDataSetProto {
	var userDataSetProto []*protoInternal.UserDataProto
	for _, elem := range userDataSet.UserData {
		userDataSetProto = append(userDataSetProto, translateToUserDataProto(elem))
	}
	return &protoInternal.UserDataSetProto{UserData: userDataSetProto}
}

// translateToUserDataSet translate []people_proto.UserDataProto to UserDataSet
func translateToUserDataSet(userDataProtoList *protoInternal.UserDataSetProto) UserDataSet  {
	var userDataList []UserData
	for _, elem := range userDataProtoList.UserData {
		userDataList = append(userDataList, translateToUserData(elem))
	}
	return UserDataSet{ UserData: userDataList }
}

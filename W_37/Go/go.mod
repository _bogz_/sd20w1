module exercise-w37

go 1.14

require (
	github.com/golang/protobuf v1.4.1
	github.com/sirupsen/logrus v1.6.0
	google.golang.org/protobuf v1.25.0
	gorm.io/driver/sqlite v1.1.2
	gorm.io/gorm v1.20.1
)

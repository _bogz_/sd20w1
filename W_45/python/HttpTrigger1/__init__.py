import logging

import azure.functions as func


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    amount = req.params.get('amount')
    name = req.params.get('name')
    tax_percent = req.params.get('tax_percent')
    if not name:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            name = req_body.get('name')
            amount = req_body.get('amount')
            tax_percent = req_body.get('tax_percent')

    if name and amount:
        return func.HttpResponse(f"Hello, {name}. Your amount after tax is {calculate_taxed_amount(amount, tax_percent)}dkk.", status_code=200)
    elif amount:
        return func.HttpResponse(
             f"You have not provided a name but your taxed amount is {calculate_taxed_amount(amount, tax_percent)}dkk",
             status_code=200
        )
    else:
        return func.HttpResponse(status_code=400)


def calculate_taxed_amount(amount, tax_percent):
    if tax_percent > 1:
        tax_percent = tax_percent / 100

    return amount - amount * tax_percent

